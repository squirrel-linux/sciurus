/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SQUIRRELLISTJOB_H
#define SQUIRRELLISTJOB_H

#include "SquirrelQueryJob.h"

class SquirrelPackagesModel;

class SquirrelListJob : public SquirrelQueryJob {
    Q_OBJECT

public:
    SquirrelListJob(QObject *parent);
    virtual void jobStarted();
    virtual void jobCompleted();
};

#endif // SQUIRRELLISTJOB_H
