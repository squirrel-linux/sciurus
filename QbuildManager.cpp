/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtDebug>

#include "QbuildManager.h"
#include "Qbuild.h"
#include "SquirrelListJob.h"
#include "SquirrelJobManager.h"
#include "SquirrelQbuildsModel.h"

QbuildManager::QbuildManager(): QObject(), m_refreshQueued(false) {
    m_qbuildsModel = new SquirrelQbuildsModel(this);
}

void QbuildManager::refresh() {

    if(!m_refreshQueued) {
        m_refreshQueued = true;

        SquirrelListJob *list = new SquirrelListJob(0);

        m_jobs->queueJob(list);
    }
}

void QbuildManager::clear() {
    foreach(Qbuild *qbuild, m_qbuilds)
        delete qbuild;

    m_qbuilds.clear();

    m_qbuildsModel->clear();
}

void QbuildManager::addQbuild(Qbuild *qbuild) {
    m_qbuilds.append(qbuild);

    m_qbuildsModel->addQbuild(qbuild);
}
