/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGWIDGET_H
#define LOGWIDGET_H

#include <QDockWidget>

#include "LogSink.h"

class QTextBrowser;

class LogWidget : public QDockWidget, public LogSink {
    Q_OBJECT

public:
    LogWidget(QWidget *parent = 0);
    virtual ~LogWidget();

    virtual void logMessage(LogManager::Severity serverity, QString message);

private:
    QTextBrowser *m_log;
};

#endif // LOGWIDGET_H
