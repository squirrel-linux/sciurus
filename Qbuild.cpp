/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Qbuild.h"

Qbuild::Qbuild(QString name, QString version): m_name(name), m_version(version), m_flags(0) {

}

QString Qbuild::toString() const {
    QStringList flags;

    if(m_flags & Built)
        flags.append("built");

    if(flags.isEmpty())
        flags.append("(none)");


    return m_name + "=" + m_version + ", use: " + m_useFlags.join(", ") + ", flags: " + flags.join(", ");
}
