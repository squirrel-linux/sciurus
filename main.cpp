/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>

#include "ManagerWindow.h"

static void loadLocales() {
    QString lang = QLocale::system().name();

    QTranslator *qtTranslator = new QTranslator(qApp);

    if(qtTranslator->load("qt_" + lang, QLibraryInfo::location(QLibraryInfo::TranslationsPath))) {
        qApp->installTranslator(qtTranslator);
    } else {
        delete qtTranslator;
    }

    QTranslator *appTranslator = new QTranslator(qApp);

    if(appTranslator->load("qsquirrel_" + lang, ":/translations")) {
        qApp->installTranslator(appTranslator);
    } else {
        delete appTranslator;
    }
}

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    app.setOrganizationName("squirrel");
    app.setApplicationName("qsquirrel");

    app.setWindowIcon(QIcon::fromTheme("system-software-install"));

    Q_INIT_RESOURCE(qsquirrel);

    loadLocales();

    ManagerWindow win;

    win.show();

    return app.exec();
}
