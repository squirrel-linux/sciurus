/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QFileDialog>

#include "ConfigurationDialog.h"
#include "ConfigurationStore.h"

#include "ui_ConfigurationDialog.h"

ConfigurationDialog::ConfigurationDialog(bool isBadPath, QWidget *parent) : QDialog(parent),
    m_ui(new Ui::ConfigurationDialog) {

    m_ui->setupUi(this);

    m_ui->badPath->setVisible(isBadPath);

    connect(this, SIGNAL(accepted()), SLOT(saveSettings()));

    loadSettings();
}

void ConfigurationDialog::loadSettings() {
    ConfigurationStore *cfg = ConfigurationStore::instance();

    m_ui->squirrelSlave->setText(cfg->squirrelPath());
}

void ConfigurationDialog::saveSettings() {
    ConfigurationStore *cfg = ConfigurationStore::instance();

    cfg->setSquirrelPath(m_ui->squirrelSlave->text());
}

ConfigurationDialog::~ConfigurationDialog() {
    delete m_ui;
}

void ConfigurationDialog::changeEvent(QEvent *e) {
    QDialog::changeEvent(e);

    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;

    default:
        break;
    }
}

void ConfigurationDialog::on_squirrelBrowse_clicked() {
    QString file = QFileDialog::getOpenFileName(this, tr("Browse for Squirrel"), QDir::currentPath(), tr("Squirrel (squirrel-slave.rb)"), 0, 0);

    if(!file.isNull()) {
        m_ui->squirrelSlave->setText(file);
    }
}
