/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QTextBrowser>
#include <QTime>

#include "LogWidget.h"
#include "LogManager.h"

LogWidget::LogWidget(QWidget *parent) : QDockWidget(parent) {
    LogManager::instance()->registerSink(this);

    setWindowTitle(tr("Build log"));
    setObjectName("LogWidget");

    m_log = new QTextBrowser(this);

    m_log->setFontFamily("monospace");

    setWidget(m_log);
}

LogWidget::~LogWidget() {
    LogManager::instance()->unregisterSink(this);
}

void LogWidget::logMessage(LogManager::Severity severity, QString message) {
    QString sevString;

    switch(severity) {
    case LogManager::Info:
        sevString = "INFO";

        break;
    case LogManager::Warning:
        sevString = "WARN";

        break;

    case LogManager::Error:
        sevString = "ERROR";

        break;

    case LogManager::Jobs:
        return;

//        sevString = "JOBS";

//        break;
    }

    QString msg = QTime::currentTime().toString(Qt::SystemLocaleLongDate) + " [" + sevString + "] " + message;

    m_log->append(msg);
}
