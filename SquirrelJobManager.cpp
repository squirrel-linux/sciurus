/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QUuid>
#include <QLocalServer>
#include <QLocalSocket>
#include <QProcess>

#include "SquirrelJobManager.h"
#include "ConfigurationStore.h"
#include "SquirrelJob.h"
#include "LogManager.h"

SquirrelJobManager::SquirrelJobManager(QObject *parent) : QObject(parent), m_socket(0), m_squirrel(0), m_accepting(false), m_currentJob(0) {
    m_server = new QLocalServer(this);

    m_socketName = "qsquirrel-" + QUuid::createUuid().toString().remove('{').split('-').first();

    m_server->removeServer(m_socketName);
    m_server->listen(m_socketName);

    connect(m_server, SIGNAL(newConnection()), SLOT(onConnection()));
}

SquirrelJobManager::~SquirrelJobManager() {
    QLocalServer::removeServer(m_socketName);
}

void SquirrelJobManager::onConnection() {
    QLocalSocket *socket = m_server->nextPendingConnection();
    socket->setParent(this);

    if(m_socket == 0) {
        m_socket = socket;

        connect(m_socket, SIGNAL(disconnected()), SLOT(onDisconnected()));
        connect(m_socket, SIGNAL(readyRead()), SLOT(onReadable()));
    } else {
        socket->abort();

        delete socket;
    }
}

void SquirrelJobManager::onDisconnected() {
    m_socket->deleteLater();

    m_socket = 0;

    m_readBuf.clear();
}

void SquirrelJobManager::startSquirrel() {
    if(m_squirrel == 0) {
        ConfigurationStore *cfg = ConfigurationStore::instance();

        QStringList tokens = cfg->squirrelPath().split('/');

        if(tokens.count() >= 3) {
            QString exe = tokens.takeLast();
            exe = tokens.takeLast() + "/" + exe;

            QString dir = tokens.join("/");

            m_squirrel = new QProcess(this);
            m_squirrel->setProcessChannelMode(QProcess::ForwardedChannels);
            m_squirrel->setWorkingDirectory(dir);

            connect(m_squirrel, SIGNAL(error(QProcess::ProcessError)), SLOT(onProcessError(QProcess::ProcessError)));
            connect(m_squirrel, SIGNAL(started()), SIGNAL(squirrelStarted()));
            connect(m_squirrel, SIGNAL(finished(int)), SLOT(onProcessTerminated()));

            m_squirrel->start(exe, QStringList("/tmp/" + m_socketName), QIODevice::ReadOnly);
        } else
            emit squirrelStartFailed();
    }
}

void SquirrelJobManager::onProcessError(QProcess::ProcessError error) {
    if(error == QProcess::FailedToStart) {
        m_squirrel->deleteLater();
        m_squirrel = 0;

        emit squirrelStartFailed();
    }
}

void SquirrelJobManager::onProcessTerminated() {
    m_squirrel->deleteLater();

    m_squirrel = 0;

    m_jobs.clear();

    emit jobsCountChanged();

    emit squirrelTerminated();
}

void SquirrelJobManager::onReadable() {
    QByteArray chunk = m_socket->readAll();

    m_readBuf += chunk;

    QList<QByteArray> rawLines = m_readBuf.split('\n');

    m_readBuf = rawLines.takeLast();

    if(m_currentJob) {
        foreach(QByteArray rawLine, rawLines) {
            if(rawLine[0] == (char) 1) {
                rawLine.remove(0, 1);

                squirrelLog(QString::fromUtf8(rawLine.data()));
            } else
                m_currentJob->squirrelWrote(QString::fromUtf8(rawLine.data()));
        }
    }

    if(m_readBuf.startsWith(QByteArray("\004#", 2))) {
        m_readBuf.clear();

        if(m_currentJob) {
            LogManager::instance()->dispatch(LogManager::Jobs, tr("Job '%1' finished").arg(m_currentJob->command()));

            disconnect(m_currentJob, SIGNAL(destroyed(QObject*)), this, SLOT(onJobDestroyed(QObject*)));

            m_jobs.removeOne(m_currentJob);

            emit jobsCountChanged();

            m_currentJob->jobCompleted();

            m_currentJob = 0;
        } else
            m_accepting = true;

        if(!m_jobs.empty()) {
            runJob();
        }
    }
}

void SquirrelJobManager::queueJob(SquirrelJob *job) {
    m_jobs.append(job);

    connect(job, SIGNAL(destroyed(QObject*)), this, SLOT(onJobDestroyed(QObject*)));

    emit jobsCountChanged();

    if(m_currentJob == 0 && m_socket != 0 && m_accepting) {
        runJob();
    }
}

void SquirrelJobManager::onJobDestroyed(QObject *obj) {
    SquirrelJob *job = static_cast<SquirrelJob *>(obj);

    m_jobs.removeOne(job);

    if(job == m_currentJob)
        m_currentJob = 0;
}

void SquirrelJobManager::squirrelLog(QString msg) {
    char level = msg[0].toAscii();

    msg.remove(0, 1);

    LogManager::Severity severity;

    switch(level) {
    case 'i':
        severity = LogManager::Info;

        break;

    case 'w':
        severity = LogManager::Warning;

        break;

    case 'e':
        severity = LogManager::Error;

        break;
    };

    LogManager::instance()->dispatch(severity, msg);
}

void SquirrelJobManager::runJob() {
    m_currentJob = m_jobs.first();

    m_currentJob->setManager(this);

    m_currentJob->jobStarted();

    m_socket->write(m_currentJob->command().toUtf8() + "\n");

    LogManager::instance()->dispatch(LogManager::Jobs, tr("Job '%1' started").arg(m_currentJob->command()));
}
