/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SQUIRRELQUERYJOB_H
#define SQUIRRELQUERYJOB_H

#include <QStringList>

#include "SquirrelJob.h"

class SquirrelQueryJob : public SquirrelJob {
    Q_OBJECT

public:
    SquirrelQueryJob(QObject *parent = 0);

    virtual void squirrelWrote(QString line);
    virtual void jobStarted();
    virtual void jobCompleted();

protected:
    QStringList m_lines;
};

#endif // SQUIRRELQUERYJOB_H
