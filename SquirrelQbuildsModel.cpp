/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QIcon>

#include "SquirrelQbuildsModel.h"
#include "SquirrelJobManager.h"
#include "SquirrelListJob.h"
#include "Qbuild.h"

SquirrelQbuildsModel::SquirrelQbuildsModel(QObject *parent) : QAbstractTableModel(parent) {

}

QVariant SquirrelQbuildsModel::data(const QModelIndex &index, int role) const {
    Qbuild *qbuild = m_qbuildsVector[index.row()];

    if(role == Qt::DisplayRole) {        
        switch(index.column()) {
        case ColumnQbuild:
            return qbuild->name();

            break;

        case ColumnVersion:
            return qbuild->version();

            break;

        case ColumnUse:
            return qbuild->useFlags().join(", ");

            break;
        }
    } else if(role == Qt::DecorationRole && index.column() == ColumnQbuild) {
        QIcon icon;

        if(qbuild->flags() & Qbuild::Built) {
            icon = QIcon::fromTheme("package-installed-updated");
        } else
            icon = QIcon::fromTheme("package-available");

        return icon;
    }

    return QVariant::Invalid;

}

int SquirrelQbuildsModel::rowCount(const QModelIndex &parent) const {
    if(parent.isValid()) {
        return 0;

    } else {
        return m_qbuildsVector.count();
    }
}

int SquirrelQbuildsModel::columnCount(const QModelIndex &parent) const {
    if(parent.isValid()) {
        return 0;

    } else {
        return Columns;
    }
}

QVariant SquirrelQbuildsModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if(role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch(section) {
        case ColumnQbuild:
            return tr("Qbuild");

            break;

        case ColumnVersion:
            return tr("Version");

            break;

        case ColumnUse:
            return tr("Use-flags");

            break;
        }
    }

    return QVariant::Invalid;
}

void SquirrelQbuildsModel::clear() {    
    if(!m_qbuildsVector.empty()) {
        beginRemoveRows(QModelIndex(), 0, m_qbuildsVector.count() - 1);

        m_qbuildsVector.clear();

        endRemoveRows();
    }
}

void SquirrelQbuildsModel::addQbuild(Qbuild *qbuild) {
    beginInsertRows(QModelIndex(), m_qbuildsVector.count(), m_qbuildsVector.count());

    m_qbuildsVector.append(qbuild);

    endInsertRows();
}
