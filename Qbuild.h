/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QBUILD_H
#define QBUILD_H

#include <QStringList>

class Qbuild {
public:
    enum {
        Built = 1,
    };

    Qbuild(QString name, QString version);

    inline QString name() const { return m_name; }
    inline QString version() const { return m_version; }

    inline int flags() const { return m_flags; }
    inline void setFlags(int flags) { m_flags = flags; }

    inline QStringList useFlags() const { return m_useFlags; }
    inline void setUseFlags(QStringList useFlags) { m_useFlags = useFlags; }

    QString toString() const;

private:
    QString m_name, m_version;
    QStringList m_useFlags;
    int m_flags;
};

#endif // QBUILD_H
