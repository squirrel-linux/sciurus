/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SquirrelListJob.h"
#include "SquirrelInfoJob.h"
#include "SquirrelJobManager.h"

#include "Qbuild.h"
#include "QbuildManager.h"

SquirrelListJob::SquirrelListJob(QObject *parent) : SquirrelQueryJob(parent) {
    setCommand("list");
}

void SquirrelListJob::jobStarted() {

}

void SquirrelListJob::jobCompleted() {
    QbuildManager::instance()->clear();

    foreach(QString line, m_lines) {
        QStringList tokens = line.split(' ');

        Qbuild *qbuild = new Qbuild(tokens[1], tokens[2]);

        if(tokens[0][0] == '+')
            qbuild->setFlags(Qbuild::Built);

        if(!tokens[3].isEmpty()) {
            qbuild->setUseFlags(tokens.mid(3, tokens.size() - 3));
        }

        QbuildManager::instance()->addQbuild(qbuild);
    }
}
