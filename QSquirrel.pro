TARGET = qsquirrel
TEMPLATE = app
QT += network
SOURCES += main.cpp \
    ManagerWindow.cpp \
    SquirrelPackagesModel.cpp \
    SquirrelJobManager.cpp \
    ConfigurationStore.cpp \
    ConfigurationDialog.cpp \
    SquirrelJob.cpp \
    LogManager.cpp \
    LogWidget.cpp \
    SquirrelBootstrapJob.cpp \
    SquirrelListJob.cpp \
    SquirrelQueryJob.cpp \
    SquirrelInfoJob.cpp \
    QbuildManager.cpp \
    Qbuild.cpp \
    SquirrelQbuildsModel.cpp
HEADERS += ManagerWindow.h \
    SquirrelPackagesModel.h \
    SquirrelJobManager.h \
    ConfigurationStore.h \
    Singleton.h \
    ConfigurationDialog.h \
    SquirrelJob.h \
    LogManager.h \
    LogSink.h \
    LogWidget.h \
    SquirrelBootstrapJob.h \
    SquirrelListJob.h \
    SquirrelQueryJob.h \
    SquirrelInfoJob.h \
    QbuildManager.h \
    Qbuild.h \
    SquirrelQbuildsModel.h
FORMS += ManagerWindow.ui \
    ConfigurationDialog.ui
TRANSLATIONS += qsquirrel_ru.ts
RESOURCES += qsquirrel.qrc
