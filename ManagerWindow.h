/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MANAGERWINDOW_H
#define MANAGERWINDOW_H

#include <QMainWindow>

namespace Ui {
    class ManagerWindow;
}

class SquirrelJobManager;
class QLabel;

class ManagerWindow : public QMainWindow {
    Q_OBJECT

public:
    ManagerWindow(QWidget *parent = 0);
    ~ManagerWindow();

protected:
    void changeEvent(QEvent *e);
    void closeEvent (QCloseEvent *event);

private slots:
    void onSquirrelStarted();
    void onSquirrelTerminated();
    void onSquirrelStartFailed();
    void on_actionPreferences_triggered();
    void onJobsCountChanged();

private:
    void saveState();
    void restoreState();

    Ui::ManagerWindow *m_ui;

    SquirrelJobManager *m_jobs;

    QLabel *m_jobsWidget;
};

#endif // MANAGERWINDOW_H
