/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SQUIRRELPACKAGESMODEL_H
#define SQUIRRELPACKAGESMODEL_H

#include <QAbstractTableModel>

class SquirrelJobManager;

class SquirrelPackagesModel : public QAbstractTableModel {
    Q_OBJECT

    enum {
        ColumnPackage = 0,
        ColumnVersion,

        Columns,
    };

public:
    SquirrelPackagesModel(SquirrelJobManager *jobs, QObject *parent = 0);

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

private:
    SquirrelJobManager *m_jobs;

    int m_rows;
};

#endif // SQUIRRELPACKAGESMODEL_H
