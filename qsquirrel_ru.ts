<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>ConfigurationDialog</name>
    <message>
        <location filename="ConfigurationDialog.ui" line="14"/>
        <source>Preferences</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="ConfigurationDialog.ui" line="24"/>
        <source>Squirrel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ConfigurationDialog.ui" line="33"/>
        <source>Cannot launch Squirrel due to invalid squirrel-slave.rb path.</source>
        <translation>Невозможно запустить Squirrel, так как путь к squirrel-slave.rb указан неверно.</translation>
    </message>
    <message>
        <location filename="ConfigurationDialog.ui" line="40"/>
        <source>Path to squirrel-slave.rb:</source>
        <translation>Путь к squirrel-slave.rb:</translation>
    </message>
    <message>
        <location filename="ConfigurationDialog.ui" line="52"/>
        <source>Browse...</source>
        <translation>Обзор...</translation>
    </message>
    <message>
        <location filename="ConfigurationDialog.cpp" line="68"/>
        <source>Browse for Squirrel</source>
        <translation>Открыть Squirrel</translation>
    </message>
    <message>
        <location filename="ConfigurationDialog.cpp" line="68"/>
        <source>Squirrel (squirrel-slave.rb)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LogWidget</name>
    <message>
        <location filename="LogWidget.cpp" line="28"/>
        <source>Build log</source>
        <translation>Журнал сборки</translation>
    </message>
</context>
<context>
    <name>ManagerWindow</name>
    <message>
        <location filename="ManagerWindow.ui" line="14"/>
        <source>Squirrel Package Manager</source>
        <translation>Менеджер пакетов Squirrel</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="25"/>
        <source>Packages</source>
        <translation>Пакеты</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="39"/>
        <source>Qbuilds</source>
        <translation>Qbuild&apos;ы</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="78"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="84"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="90"/>
        <source>Edit</source>
        <translation>Редактирование</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="103"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="119"/>
        <source>Quit</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="122"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="127"/>
        <source>Preferences</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="132"/>
        <source>Reload</source>
        <translation>Перезагрузить</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="135"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="140"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="ManagerWindow.ui" line="143"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ManagerWindow.cpp" line="100"/>
        <source>Squirrel has been unexpectedly terminated. Try to restart it?</source>
        <translation>Процесс Squirrel неожиданно завершился. Попытаться его перезапустить?</translation>
    </message>
    <message>
        <location filename="ManagerWindow.cpp" line="115"/>
        <source>Jobs: %1</source>
        <translation>Задач: %1</translation>
    </message>
    <message>
        <location filename="ManagerWindow.cpp" line="126"/>
        <source>Squirrel is processing job. Are you sure?</source>
        <translation>Squirrel выполняет задачу. Вы уверены?</translation>
    </message>
</context>
<context>
    <name>SquirrelJobManager</name>
    <message>
        <location filename="SquirrelJobManager.cpp" line="139"/>
        <source>Job &apos;%1&apos; finished</source>
        <translation>Задача &apos;%1&apos; выполнена</translation>
    </message>
    <message>
        <location filename="SquirrelJobManager.cpp" line="216"/>
        <source>Job &apos;%1&apos; started</source>
        <translation>Задача &apos;%1&apos; запущена</translation>
    </message>
</context>
<context>
    <name>SquirrelPackagesModel</name>
    <message>
        <location filename="SquirrelPackagesModel.cpp" line="59"/>
        <source>Package</source>
        <translation>Пакет</translation>
    </message>
    <message>
        <location filename="SquirrelPackagesModel.cpp" line="64"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
</context>
<context>
    <name>SquirrelQbuildsModel</name>
    <message>
        <location filename="SquirrelQbuildsModel.cpp" line="87"/>
        <source>Qbuild</source>
        <translation></translation>
    </message>
    <message>
        <location filename="SquirrelQbuildsModel.cpp" line="92"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="SquirrelQbuildsModel.cpp" line="97"/>
        <source>Use-flags</source>
        <translation>Use-флаги</translation>
    </message>
</context>
</TS>
