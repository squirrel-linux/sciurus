/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SQUIRRELJOBMANAGER_H
#define SQUIRRELJOBMANAGER_H

#include <QObject>
#include <QProcess>

class QLocalServer;
class QLocalSocket;

class SquirrelJob;

class SquirrelJobManager : public QObject {
    Q_OBJECT

public:
    SquirrelJobManager(QObject *parent = 0);
    virtual ~SquirrelJobManager();

    void startSquirrel();
    void queueJob(SquirrelJob *job);

    inline int jobsCount() const { return m_jobs.count(); }

private slots:
    void onConnection();
    void onDisconnected();
    void onReadable();
    void onJobDestroyed(QObject *obj);

    void onProcessError(QProcess::ProcessError error);
    void onProcessTerminated();

signals:
    void squirrelStarted();
    void squirrelTerminated();
    void squirrelStartFailed();
    void jobsCountChanged();

private:
    void squirrelLog(QString msg);
    void runJob();

    QLocalServer *m_server;
    QLocalSocket *m_socket;
    QProcess *m_squirrel;
    QString m_socketName;
    QByteArray m_readBuf;
    QList<SquirrelJob *> m_jobs;
    bool m_accepting;
    SquirrelJob *m_currentJob;
};

#endif // SQUIRRELJOBMANAGER_H
