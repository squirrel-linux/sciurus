/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SQUIRRELJOB_H
#define SQUIRRELJOB_H

#include <QObject>

class SquirrelJobManager;

class SquirrelJob : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString command READ command);

public:
    SquirrelJob(QObject *parent = 0);

    inline QString command() const { return m_command; }

    virtual void squirrelWrote(QString line);
    virtual void jobStarted() = 0;
    virtual void jobCompleted() = 0;

    inline void setManager(SquirrelJobManager *manager) { m_manager = manager; }

protected:
    inline SquirrelJobManager *manager() const { return m_manager; }
    inline void setCommand(QString command) { m_command = command; }

private:
    QString m_command;
    SquirrelJobManager *m_manager;
};

#endif // SQUIRRELJOB_H
