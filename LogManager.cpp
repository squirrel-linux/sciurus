/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LogSink.h"
#include "LogManager.h"

void LogManager::registerSink(LogSink *sink) {
    m_sinks.append(sink);
}

void LogManager::unregisterSink(LogSink *sink) {
    m_sinks.removeOne(sink);
}

void LogManager::dispatch(Severity severity, QString message) {
    foreach(LogSink *sink, m_sinks) {
        sink->logMessage(severity, message);
    }
}
