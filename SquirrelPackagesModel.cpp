/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SquirrelPackagesModel.h"
#include "SquirrelJobManager.h"
#include "SquirrelListJob.h"

SquirrelPackagesModel::SquirrelPackagesModel(SquirrelJobManager *jobs, QObject *parent) : QAbstractTableModel(parent), m_jobs(jobs), m_rows(0) {

}

QVariant SquirrelPackagesModel::data(const QModelIndex &index, int role) const {
    Q_UNUSED(index);

    if(role == Qt::DisplayRole) {
        return "trololo";
    } else {
        return QVariant::Invalid;
    }
}

int SquirrelPackagesModel::rowCount(const QModelIndex &parent) const {
    if(parent.isValid()) {
        return 0;

    } else {
        return m_rows;
    }
}

int SquirrelPackagesModel::columnCount(const QModelIndex &parent) const {
    if(parent.isValid()) {
        return 0;

    } else {
        return Columns;
    }
}

QVariant SquirrelPackagesModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if(role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch(section) {
        case ColumnPackage:
            return tr("Package");

            break;

        case ColumnVersion:
            return tr("Version");

            break;
        }
    }

    return QVariant::Invalid;
}
