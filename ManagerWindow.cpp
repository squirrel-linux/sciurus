/*
 * GUI For Squirrel - Tool for automated building of (embedded) Linux packages
 * Copyright (C) 2010  Sergey Gridassov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtDebug>

#include <QLabel>
#include <QMessageBox>
#include <QCloseEvent>

#include "ManagerWindow.h"
#include "QbuildManager.h"
#include "SquirrelJobManager.h"
#include "ConfigurationDialog.h"
#include "LogWidget.h"
#include "ConfigurationStore.h"
#include "SquirrelQbuildsModel.h"

#include "SquirrelBootstrapJob.h"

#include "ui_ManagerWindow.h"

ManagerWindow::ManagerWindow(QWidget *parent) : QMainWindow(parent),
    m_ui(new Ui::ManagerWindow) {

    m_ui->setupUi(this);

    m_ui->actionQuit->setIcon(QIcon::fromTheme("application-exit"));
    m_ui->actionPreferences->setIcon(QIcon::fromTheme("preferences-desktop"));

    addDockWidget(Qt::BottomDockWidgetArea, new LogWidget(this), Qt::Horizontal);

    restoreState();

    m_jobs = new SquirrelJobManager(this);

    connect(m_jobs, SIGNAL(jobsCountChanged()), SLOT(onJobsCountChanged()));

    connect(m_jobs, SIGNAL(squirrelStarted()), SLOT(onSquirrelStarted()));
    connect(m_jobs, SIGNAL(squirrelTerminated()), SLOT(onSquirrelTerminated()));
    connect(m_jobs, SIGNAL(squirrelStartFailed()), SLOT(onSquirrelStartFailed()));

    QbuildManager::instance()->setJobs(m_jobs);

    m_ui->qbuilds->setModel(QbuildManager::instance()->qbuildsModel());

    m_jobsWidget = new QLabel(this);

    m_ui->statusbar->addPermanentWidget(m_jobsWidget);

    onJobsCountChanged();

    m_jobs->startSquirrel();

    SquirrelBootstrapJob *bootstrap = new SquirrelBootstrapJob(this);

    m_jobs->queueJob(bootstrap);

    QbuildManager::instance()->refresh();
}

ManagerWindow::~ManagerWindow() {
    delete m_ui;
}

void ManagerWindow::changeEvent(QEvent *e) {
    QMainWindow::changeEvent(e);

    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;

    default:
        break;
    }
}

void ManagerWindow::onSquirrelStarted() {

}

void ManagerWindow::onSquirrelTerminated() {
    if(QMessageBox::question(this, windowTitle(), tr("Squirrel has been unexpectedly terminated. Try to restart it?"), QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes) {
        m_jobs->startSquirrel();
    }
}

void ManagerWindow::onSquirrelStartFailed() {
    ConfigurationDialog cfg(true, this);

    if(cfg.exec() == QDialog::Rejected)
        close();
    else
        m_jobs->startSquirrel();
}

void ManagerWindow::onJobsCountChanged() {
    m_jobsWidget->setText(tr("Jobs: %1").arg(m_jobs->jobsCount()));
}

void ManagerWindow::on_actionPreferences_triggered() {
    ConfigurationDialog config(false, this);

    config.exec();
}

void ManagerWindow::closeEvent(QCloseEvent *event) {
    if(m_jobs->jobsCount() > 0) {
        if(QMessageBox::question(this, windowTitle(), tr("Squirrel is processing job. Are you sure?"), QMessageBox::Yes, QMessageBox::No) == QMessageBox::No) {
            event->ignore();

            return;
        }
    }

    saveState();
}

void ManagerWindow::saveState() {
    QByteArray state;

    QDataStream stream(&state, QIODevice::WriteOnly);

    stream << QMainWindow::saveState();
    stream << saveGeometry();

    ConfigurationStore::instance()->setWindowState(state);
}

void ManagerWindow::restoreState() {
    QByteArray state = ConfigurationStore::instance()->windowState();

    if(state.isEmpty())
        return;

    QDataStream stream(&state, QIODevice::ReadOnly);

    QByteArray winState, geometry;

    stream >> winState >> geometry;

    QMainWindow::restoreState(winState);
    restoreGeometry(geometry);
}
